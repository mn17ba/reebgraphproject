#ifndef GEODESICS_H
#define GEODESICS_H

#include "mesh.h"
#include <string>

// Functions to calculate the geodesics
bool calculateGeodesics(Mesh &mesh);
std::vector<float> dijkstras(Mesh &mesh, int sV);
float distance(const Point3D &a, const Point3D &b);

#endif // GEODESICS_H

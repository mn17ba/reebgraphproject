#ifndef MESH_H
#define MESH_H

#include <vector>
#include <string>
#include <cmath>
#include "point3d.h"

// Class to represent the directed edge mesh data structure
class Mesh
{
public:
    Mesh();
    ~Mesh();

public:
    std::string fileName;

    std::vector<Point3D> position;          // All vertices in mesh
    std::vector<int> firstDirectedEdge;     // per vertex
    std::vector<int> faceVertices;          // Target vertices of a directed-edge
    std::vector<int> otherHalf;             // Index of opposite directed-edge

    // Store the piecewise linear geodesics for a mesh
    std::vector<float> geodesics;

    // Store the colours of each point in a vector
    std::vector<float> colours;

    // Store the type of each point in a vector
    std::vector<PointType> pointTypes;

    // Store the PL function as a vector storing a pair.
    // The first in pair is the geodesics value of the point so it can be sorted easily
    // The second in the pair is the vertexID
    std::vector< std::pair<float, int> > plFunction;

public:
    // Read in a mesh from a OFF file
    int readOFFFile(const std::string &fileName);
    int readGeodesicsFile(const std::string &fileName);

    /* TRAVERSAL OPERATIONS */
    // e is the index of the directed-edge.
    int prev(int e) const { return (e % 3 == 0) ? e+2 : e-1; }
    int next(int e) const { return (e % 3 == 2) ? e-2 : e+1; }

    // Faces represent 3 CCW directed-edges.
    int faceIndex(int e) const { return e / 3; }

    // Returns the Neighbours of a vertex
    std::vector<int> getLink(int v) const;

    // Returns adjacent faces of a vertex
    std::vector<int> getStar(int v) const;


    // Returns the Neighbours of a vertex u
    // where geodesics[u] < geodesics[v]
    std::vector<int> getLowerLink(int v) const;

    // Reeb graph operations
    void setPointTypes();
    void setupPLFunction();

    // Draw the mesh in OpenGL.
    bool setHeatMapColours();
    bool setTransparent();
    void render();

    // For debugging
    void printPLFunction();
};


#endif // MESH_H

#include <QApplication>
#include <QVBoxLayout>
#include "window.h"

int main(int argc, char *argv[]) {

	QApplication app(argc, argv);
  	MainWindow *window = new MainWindow(NULL);
	window->show();
	app.exec();

	delete window;
	return 0;
}

#if defined(__linux__)
  #include <GL/glu.h>
#elif defined(__APPLE__) && defined(__MACH__)
  #include <OpenGL/glu.h>
#endif

#include <QDebug>
#include <fstream>
#include <string>
#include <iostream>

#include "mesh.h"

static void GLClearError()
{
    while (glGetError() != GL_NO_ERROR) {}
}

static void GLCheckError()
{
    while (GLenum error = glGetError())
    {
        //qDebug() << "[OpenGL Error] (" << error << ")";
    }
}

Mesh::Mesh()
{
}

Mesh::~Mesh()
{
}

// Sets low values to red, midrange values to green, extremities to blue
bool Mesh::setHeatMapColours()
{
    if(geodesics.empty())
    {
        return false;
    }

    if(!colours.empty()){
        colours.clear();
    }

    float max = 0.0f, min = 999999.0f;

    for(const float &value : geodesics)
    {
        if (value > max) max = value;
        if (value < min) min = value;
    }

    float range = max - min;

    // Colour distribution is red-green-blue
    for(const float &value : geodesics)
    {
        float ratio = 2 * (value - min) / range;
        float red = (0 > 1 - ratio) ? 0 : 1 - ratio;
        float blue =  (0 > ratio - 1) ? 0 : ratio - 1;
        float green = 1 - red - blue;

        colours.push_back(red);
        colours.push_back(green);
        colours.push_back(blue);
        colours.push_back(1.0f);
    }
    return true;
}

// S
bool Mesh::setTransparent()
{
    if(!colours.empty()){
        colours.clear();
    }

    for(size_t i = 0; i < position.size(); ++i)
    {
        colours.push_back(1.0f);
        colours.push_back(1.0f);
        colours.push_back(1.0f);
        colours.push_back(0.3f);
    }

    return true;
}

int Mesh::readOFFFile(const std::string &fileName)
{
    this->fileName = fileName;
    size_t lastindex = this->fileName.find_last_of(".");
    this->fileName = this->fileName.substr(0, lastindex);

    std::ifstream file(fileName);

    if(!file.is_open())
    {
        return 0;
    }

    if(!position.empty())
    {
        position.clear();
        faceVertices.clear();
        firstDirectedEdge.clear();
        otherHalf.clear();
        geodesics.clear();
        colours.clear();
        plFunction.clear();
        pointTypes.clear();
    }

    std::string input;
    file >> input; // Read OFF

    if(input.compare("OFF") != 0)
    {
        return 0;
    }

    int numVertices, numFaces;
    file >> numVertices >> numFaces;
    file >> input; // Read numEdges (0)

    for(int i = 0; i < numVertices; ++i)
    {
        float x, y, z;
        file >> x >> y >> z; // Read x, y, z coordinates

        position.push_back(Point3D(x, y, z));
    }

    for(int i = 0; i < numFaces; ++i)
    {
        file >> input; // 3 (assuming triangle mesh)

        int v0, v1, v2;
        file >> v0 >> v1 >> v2; // read face vertices

        faceVertices.push_back(v0);
        faceVertices.push_back(v1);
        faceVertices.push_back(v2);
    }

    // Get first directed edges
    for(int vertexID = 0; vertexID < numVertices; ++vertexID)
    {
        int n = 0;
        while(vertexID != faceVertices[n]) n++;
        firstDirectedEdge.push_back(next(n));
    }

    // Get opposite edges
    for(size_t edgeID = 0; edgeID < faceVertices.size(); ++edgeID)
    {
        size_t n = 0;
        int v0 = faceVertices[prev(edgeID)];
        int v1 = faceVertices[edgeID];

        while(((v1 != faceVertices[n]) || (v0 != faceVertices[next(n)])) && (n != faceVertices.size())) {
            n++;
        }

        if(n == faceVertices.size()){
            std::cout << "Error: Mesh has boundary." << std::endl;
            position.clear();
            return 0;
        }

        otherHalf.push_back(next(n));
    }

    if(!geodesics.empty() || !colours.empty())
    {
        geodesics.clear();
        colours.clear();
    }

    // file read successful
    return 1;
}

// Executes when user clicks button to set geodesics. Reads fileName
// and sets the geodesics of the mesh. Calls setHeatMapColours to
// set a colour array for rendering a heatmap.
int Mesh::readGeodesicsFile(const std::string &fileName)
{
    std::ifstream file(fileName);

    if(!file.is_open())
    {
        return 0;
    }

    if(!geodesics.empty())
    {
        geodesics.clear();
    }

    int i = 0;
    float input;
    while(file >> input)
    {
        geodesics.push_back(input);
        plFunction.push_back(std::make_pair(input, i));
        i++;
    }

    setHeatMapColours();

    return 1;
}

void Mesh::render()
{
    GLClearError();

    if(!geodesics.empty())
    {
        glEnableClientState( GL_COLOR_ARRAY );
        glColorPointer( 4, GL_FLOAT, 0, &colours[0] );
    }

    glEnableClientState(GL_VERTEX_ARRAY);
    glVertexPointer(3, GL_FLOAT, 0, &position[0]);
    glDrawElements(GL_TRIANGLES, faceVertices.size(), GL_UNSIGNED_INT, &faceVertices[0]);
    glDisableClientState(GL_VERTEX_ARRAY);

    glDisableClientState(GL_COLOR_ARRAY);

    GLCheckError();
}

// Returns the neighbouring vertices for vertex at index v
std::vector<int> Mesh::getLink(int v) const
{
    std::vector<int> vArr;
    int firstEdge = firstDirectedEdge[v];
    vArr.push_back(faceVertices[firstEdge]);

    int e = otherHalf[firstEdge];
    e = next(e);

    while( e != firstEdge ) {
        vArr.push_back(faceVertices[e]);
        e = otherHalf[e];
        e = next(e);
    }

    return vArr;
}



std::vector<int> Mesh::getStar(int v) const
{
    std::vector<int> vArr;
    int firstEdge = firstDirectedEdge[v];
    vArr.push_back(firstEdge / 3);

    int e = otherHalf[firstEdge];
    e = next(e);

    while( e != firstEdge ) {
        vArr.push_back(e / 3);
        e = otherHalf[e];
        e = next(e);
    }

    return vArr;
}

std::vector<int> Mesh::getLowerLink(int v) const
{
    float maxVal = geodesics[v];

    std::vector<int> link = getLink(v);

    auto it = link.begin();
    while(it != link.end())
    {
        if(geodesics[link[it - link.begin()]] > maxVal)
        {
            link.erase(it);
        }
        else {
            ++it;
        }
    }

    return link;
}

// Labels the points in a mesh with their point type
// i.e. a vertex is either a regular, minimum, maximum or saddle point
void Mesh::setPointTypes()
{
    if(!pointTypes.empty())
    {
        pointTypes.clear();
    }

    for(size_t v = 0; v < position.size(); ++v)
    {
        std::vector<int> link = getLink(v);
        std::vector<int> lowerLink = getLowerLink(v);

        if(lowerLink.size() == link.size())
        {
            pointTypes.push_back(PointType::maximum);
            continue;
        }
        else if(lowerLink.size() == 0)
        {
            pointTypes.push_back(PointType::minimum);
            continue;
        }
        // regular if lower-link is a non-empty connected subset of the link
        // Concatenate link onto the end of link
        link.insert( link.end(), link.begin(), link.end() );

        // Search for lowerLink inside link
        // Have to check each cyclic permutation
        std::vector<std::vector<int>> lowerLinkPermutations;
        for(size_t i = 0; i < lowerLink.size(); ++i)
        {
            std::vector<int> perm;
            for(size_t j = 0; j < lowerLink.size(); ++j)
            {
                int index = (j + i) % lowerLink.size();
                perm.push_back(lowerLink[index]);
            }
            lowerLinkPermutations.push_back(perm);
        }

        size_t j = 0;
        bool flag = false;

        // Iterate through the permutations of the lower link to check
        // if it's a regular point or a saddle point
        for(const auto &perm : lowerLinkPermutations)
        {
            for(size_t i = 0; i < link.size(); ++i)
            {
                if (j == perm.size()) break;
                j = (perm[j] == link[i]) ? j + 1 : 0;
            }
            if(j == lowerLink.size())
            {
                pointTypes.push_back(PointType::regular);
                flag = true;
                break;
            }
        }

        if(!flag)
        {
            pointTypes.push_back(PointType::saddle);
        }
    }
}

// Sorts the PL geodesics function in order of increasing size
void Mesh::setupPLFunction()
{
    if(plFunction.empty())
    {
        return;
    }

    // STL sort function utilized
    std::sort(plFunction.begin(), plFunction.end());

}

// Print all values in PL function for debugging purposes
void Mesh::printPLFunction()
{
    for (const auto &value : plFunction)
    {
        std::cout << "ID: " << value.second << "\tD: " << value.first << std::endl;
    }
}

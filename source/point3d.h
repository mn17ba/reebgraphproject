#ifndef POINT3D_H
#define POINT3D_H

// Class to represent a point in 3D space
class Point3D
{
public:
    Point3D(float x, float y, float z);
    Point3D();

    float x, y, z;
};

// Enum class to represent a critical point type
enum class PointType {
    regular = -1,
    minimum,
    saddle,
    maximum,
};

#endif // POINT3D_H

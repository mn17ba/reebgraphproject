#ifndef SWEEP_H
#define SWEEP_H

#include<vector>

#include "mesh.h"
#include "cycle.h"
#include "reebgraph.h"

// Class used to implement the sweep algorithm
class Sweep {
public:
    Sweep();
    bool sweep(const Mesh &mesh);
    ReebGraph *reebGraph;

private:
    int contourNumber = 0;
    int peakMemoryUsed = 0;
    std::vector<CyclicList*> cyclicLists;

private:
    //
    bool regular(const Mesh &mesh, int v);
    void minimum(const Mesh &mesh, int v);
    void maximum(const Mesh &mesh, int v);
    bool saddle(const Mesh &mesh, int v);

    void printCycles();
    int calculateMemoryUsage(); // Gets the number of faces in a level set to measure memory performance
};

#endif // SWEEP_H

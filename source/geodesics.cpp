#include <queue>
#include <cmath>
#include <vector>
#include <array>
#include <chrono>
#include <iostream>
#include <fstream>
#include <QTextStream>
#include <QDebug>

#include "geodesics.h"

// Takes in a mesh, calculates the PL function for geodesics and writes result to fileName.geodesics
bool calculateGeodesics(Mesh &mesh)
{
    std::vector< std::vector<float> > allDistances;

    // Open a .geodesics file to write the average geodesics to, so they
    // only have to be computed once.
    std::ofstream file(mesh.fileName + ".geodesics");
    if (!file)
    {
        // Error file couldn't be created.
        return false;
    }

    auto start = std::chrono::high_resolution_clock::now();

    // Push the outputs of dijkstra's, for each vertex as the source point.
    for(size_t i = 0; i < mesh.position.size(); ++i)
    {
        //std::cout << "Calculating dijksta's on point: " << i << " / " << (int) mesh.position.size() - 1 << std::endl;
        allDistances.push_back(dijkstras(mesh, i));
    }

    // Calculate the average geodesic value for each vertex.
    for(size_t j = 0; j < mesh.position.size(); ++j)
    {
        float totalDistance = 0.0f;
        for(size_t i = 0; i < allDistances.size(); ++i)
        {
            totalDistance += allDistances[i][j];
        }
        float averageGeodesic = totalDistance / allDistances.size();
        mesh.geodesics.push_back(averageGeodesic); // The geodesics are stored in the mesh object.
        mesh.plFunction.push_back(std::make_pair(averageGeodesic, j));

        file << averageGeodesic << std::endl;
    }

    auto stop = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
    std::cout << "Geodesics execution time: " << duration.count() << " microseconds" << std::endl;

    file.close();
    return true;
}

// SSP Algorithm, Dijkstra's, implemented with the STL priority queue as a min heap.
std::vector<float> dijkstras(Mesh &mesh, int sV)
{
    if(mesh.position.size() == 0)
    {
        return std::vector<float>();
    }

    const float MAX = 9999999.0f;
    std::vector<float> dist(mesh.position.size(), MAX);

    typedef std::pair<float, int> heapPair;
    std::priority_queue<heapPair, std::vector<heapPair>, std::greater< heapPair> > minHeap;

    minHeap.push(std::make_pair(0, sV));
    dist[sV] = 0;

    std::vector<int> vArr;

    while(!minHeap.empty())
    {
        int v = minHeap.top().second;
        minHeap.pop();

        vArr = mesh.getLink(v);

        for (size_t i = 0; i < vArr.size(); ++i)
        {
            float d = distance(mesh.position[v], mesh.position[vArr[i]]);

            if( (dist[v] + d) < dist[ vArr[i] ] )
            {
                dist[vArr[i]] = dist[v] + d;
                minHeap.push(std::make_pair(dist[ vArr[i] ], vArr[i]) );
            }
        }
    }

    return dist;
}

// Returns the Euclidean distance between two points a and b.
float distance(const Point3D &a, const Point3D &b)
{
    return std::sqrt(std::pow(a.x - b.x, 2) +
                     std::pow(a.y - b.y, 2) +
                     std::pow(a.z - b.z, 2));
}

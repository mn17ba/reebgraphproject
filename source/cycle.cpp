//#include <QDebug>

#include "cycle.h"
#include <iostream>

// Constructor
CyclicList::CyclicList()
    : last(nullptr)
{
}

// Constructor
CyclicList::CyclicList(int contourNumber)
    : last(nullptr), contourNumber(contourNumber)
{
}

// Destructor - frees all memory that was allocated for nodes in the list
CyclicList::~CyclicList()
{
    if (last == nullptr)
        return;

    Node* current = last->next;
    Node* next;

    do
    {
       next = current->next;
       delete current;
       current = next;
    }
    while(current != last);

    delete last;
}

// Return a pointer to the next node in the contour cycle
// bool reverse is used to change this function to getPrev for ease of use during sweep
Node* CyclicList::getNext(Node *node, bool reverse) const
{
    if(!reverse)
        return node->next;
    else
        return node->prev;
}

// Return a pointer to the prev node in the contour cycle
// bool reverse is used to change this function to getNext for ease of use during sweep
Node* CyclicList::getPrev(Node *node, bool reverse) const
{
    if(!reverse)
        return node->prev;
    else
        return node->next;
}

// Used to insert the first face into a contour cycle
Node* CyclicList::insertToEmpty(int faceId)
{
    if (last != nullptr)
        return last;

    last = new Node(faceId);
    last->next = last;
    last->prev = last;

    return last;
}

// Used to insert a face into the contour cycle after the last element
Node* CyclicList::insert(int faceId)
{
    if(last == nullptr)
    {
        return insertToEmpty(faceId);
    }

    Node* temp = new Node(faceId);

    temp->next = last->next;
    temp->prev = last;

    last->next = temp;
    last->next->next->prev = temp;

    return last;
}

// Used to insert a face into the contour cycle after the node specified in the arguments
// bool reverse changes the function to insert before the node instead for ease of use during sweep algorithm
void CyclicList::insertAfter(Node* node, int faceId, bool reverse)
{
    if(!reverse){
        Node* temp = new Node(faceId);

        temp->next = node->next;
        temp->prev = node;

        node->next = temp;
        temp->next->prev = temp;
    } else
    {
        Node* temp = new Node(faceId);

        temp->prev = node->prev;
        temp->next = node;

        node->prev = temp;
        temp->prev->next = temp;
    }
}

// Used to delete a face into the contour cycle after the node specified in the arguments
// bool reverse changes the function to delete before the node instead for ease of use during sweep algorithm
void CyclicList::deleteAfter(Node* node, bool reverse)
{
    if(!reverse)
    {
        last = node->prev;

        Node *temp = node->next;
        node->next = temp->next;
        temp->next->prev = node;
        delete temp;
    } else
    {
        last = node->next;

        Node *temp = node->prev;
        node->prev = temp->prev;
        temp->prev->next = node;
        delete temp;
    }
}

// Goes through a contour cycle and returns a node that matches the faceId
// in the arguments, this is a sequential linear search.
Node* CyclicList::find(int faceId) const
{
    if (last == nullptr)
    {
        return nullptr;
    }

    Node* temp = last->next;
    do
    {
        if (temp->faceId == faceId) return temp;
        temp = temp->next;
    }
    while(temp != last->next);

    return nullptr;
}

// Print the contour cycle for debugging purposes
void CyclicList::print() const
{
    if (last == nullptr)
    {
        std::cout << "Empty" << std::endl;
        return;
    }

    std::cout << "[ ";
    Node* temp = last->next;
    do
    {
        std::cout << temp->faceId << " ";
        temp = temp->next;
    }
    while(temp != last->next);
    std::cout << "]" << std::endl;
}

// Print the contour cycle in full for debugging purposes
void CyclicList::printFull() const
{
    if (last == nullptr)
    {
        std::cout << "Empty" << std::endl;
        return;
    }

    Node* temp = last->next;
    do
    {
        std::cout << "face: " << temp->faceId << ", next: " << temp->next->faceId << ", prev: " << temp->prev->faceId << std::endl;
        temp = temp->next;
    }
    while(temp != last->next);
    std::cout << "]" << std::endl;
}

// Returns the number of faces in a contour cycle
int CyclicList::getNumFaces() const
{
    if (last == nullptr)
    {
        return 0;
    }

    int numFaces = 0;

    Node* temp = last->next;
    do
    {
        temp = temp->next;
        numFaces += 1;
    }
    while(temp != last->next);

    return numFaces;
}

// Gets the star of a vertex in the form of a contour cycle
bool CyclicList::getStar(const Mesh &mesh, int v)
{
    std::vector<int> faceList = mesh.getStar(v);

    if(faceList.empty())
    {
        return false;
    }

    for(const int &faceId : faceList)
    {
        last = insert(faceId);
    }

    return true;
}





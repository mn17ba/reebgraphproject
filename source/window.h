#ifndef WINDOW_H
#define WINDOW_H

#include <QGLWidget>
#include <QBoxLayout>
#include <QLabel>
#include <QSlider>
#include <QPushButton>
#include <QFileDialog>


#include "display.h"

class MainWindow: public QWidget {
Q_OBJECT
public:
       
    MainWindow(QWidget *parent);
    ~MainWindow();

    QVBoxLayout *windowLayout;
    QHBoxLayout *buttonLayout;

    // OPENGL display
    DisplayWidget *display;
//    QPushButton *refreshDisplayButton;

    // Rotate object controls
    QLabel *xAxisLabel;
    QLabel *yAxisLabel;
    QLabel *zAxisLabel;
    QSlider *xAxisSlider;
    QSlider *yAxisSlider;
    QSlider *zAxisSlider;
    QLabel *zoomLabel;
    QSlider *zoomSlider;

    // Files
    QString *fileName;
    QPushButton *fileButton;

    // Geodesics
    QPushButton *calcGeodesicsButton;
    QPushButton *setGeodesicsButton;

//    // Critical points
//    QPushButton *setCriticalButton;

    // Sweep
    QPushButton *sweepButton;

	// resets all the interface elements
	void ResetInterface();
    void closeEvent(QCloseEvent *event);

public slots:
    void handleFileButton();
    void handleSetGeodesics();

};
	
#endif

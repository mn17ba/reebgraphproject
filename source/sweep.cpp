#include <iostream>

#include <QDebug>
#include <chrono>
#include <stdlib.h>

#include "sweep.h"
#include "cycle.h"


Sweep::Sweep()
    : contourNumber(0)
{
    reebGraph = new ReebGraph();
}

// Function to initiate a sweep of the vertices of a mesh, building the Reeb graph as we go.
bool Sweep::sweep(const Mesh &mesh)
{
    auto start = std::chrono::high_resolution_clock::now();

    int count = 0;
    for(const auto &pair : mesh.plFunction)
    {
        int v = pair.second;

        //std::cout << "vertex v: " << v << ", geodesic: " << mesh.geodesics[v] << std::endl;

        // Check what type of point is currently being processed
        if(mesh.pointTypes[v] == PointType::regular)
        {
            regular(mesh, v);

        } else if (mesh.pointTypes[v] == PointType::minimum)
        {
            minimum(mesh, v);

        } else if (mesh.pointTypes[v] == PointType::maximum)
        {
            maximum(mesh, v);

        }  else if (mesh.pointTypes[v] == PointType::saddle)
        {
            saddle(mesh, v);

        }
        count++;

    }

//    std::cout << "peak memory usage: " << peakMemoryUsed << std::endl;

    // Measure the execution time of a sweep
    auto stop = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
    std::cout << "Sweep execution time: " << duration.count() << " microseconds" << std::endl;

    return true;
}

bool Sweep::regular(const Mesh &mesh, int v)
{
    CyclicList star = CyclicList();
    star.getStar(mesh, v);

    // Find a face id in the cycliclists.
    Node *contourFace = NULL, *firstOcc = NULL;
    CyclicList *contour = NULL;

    Node *starFace = star.last->next;
    bool reverse = false;

    // Find the first triangle and the last triangle, from the star in the level-set
    do
    {
        for(const auto &cycle : cyclicLists)
        {
            contourFace = cycle->find(starFace->faceId);

            if (contourFace!=NULL) {

                if ((contourFace->prev->faceId != starFace->prev->faceId)
                  && (contourFace->next->faceId == starFace->next->faceId))
                {
                    contour = cycle;
                    firstOcc = contourFace;
                    break;
                 }

                if ((contourFace->prev->faceId == starFace->next->faceId)
                  && (contourFace->next->faceId != starFace->prev->faceId))
                {
                    contour = cycle;
                    firstOcc = contourFace;
                    reverse = true;
                    break;
                 }
            }
        }
        if (firstOcc!=NULL) {
            break;
        }
        starFace = starFace->next;
    } while (starFace != star.last->next);

    // point not found
    if(firstOcc == NULL)
    {
        std::cout << "Error: point " << v << " not found" << std::endl;
        return false;
    }

    // Delete faces in lower star
    while(contour->getNext(contour->getNext(firstOcc, reverse), reverse)->faceId == starFace->next->next->faceId)
    {
        contour->deleteAfter(firstOcc, reverse);
        starFace = starFace->next;
    }

    starFace = starFace->next->next;

    // Insert faces in upper star
    while(starFace->faceId != firstOcc->faceId)
    {
        contour->insertAfter(firstOcc, starFace->faceId, reverse);
        starFace = starFace->next;
     }

    return true;
}

bool Sweep::saddle(const Mesh &mesh, int v)
{
    CyclicList star = CyclicList();
    star.getStar(mesh, v);

    //std::cout << "star: ";
    //star.print();

    std::vector<CyclicList*> contours;
    std::vector<Node*> firstOcc;
    std::vector<Node*> starFirst;
    std::vector<Node*> starLast;
    std::vector<Node*> lastOcc;
    std::vector<bool> reverse;
    Node *contourFace = NULL;
    int contourId = 0;

    Node *starFace = star.last->next;

    // Loop through faces in the star of the vertex and search for it in the level-set
    do
    {
        contourId = 0;
        for(const auto &cycle : cyclicLists)
        {
            contourFace = cycle->find(starFace->faceId);

            if (contourFace!=NULL) {
                if ((contourFace->prev->faceId != starFace->prev->faceId)
                  && (contourFace->next->faceId == starFace->next->faceId))
                {
                    firstOcc.push_back(contourFace);
                    starFirst.push_back(starFace);
                    reverse.push_back(false);

                    if(contours.empty() || contours.back() != cycle)
                    {
                        contours.push_back(cycle);
                    }
                }

                if ((contourFace->prev->faceId == starFace->next->faceId)
                  && (contourFace->next->faceId != starFace->prev->faceId))
                {
                    firstOcc.push_back(contourFace);
                    starFirst.push_back(starFace);
                    reverse.push_back(true);
                    if(contours.empty() || contours.back() != cycle)
                    {
                        contours.push_back(cycle);
                    }
                }
                if(firstOcc.size() == 2)
                {
                    break;
                }
            }
            contourId++;
        }
        if(firstOcc.size() == 2)
        {
            break;
        }
        starFace = starFace->next;
    } while(starFace != star.last->next);

    // Coutour SPLIT condition
    if (contours.size() == 1) // If size is 1, the contours split into two.
    {

        for(size_t i = 0; i < firstOcc.size(); ++i)
        {
            while(contours[0]->getNext(contours[0]->getNext(firstOcc[i], reverse[i]), reverse[i])->faceId
                        == starFirst[i]->next->next->faceId)
            {
                contours[0]->deleteAfter(firstOcc[i], reverse[i]);
                star.deleteAfter(starFirst[i], false);
            }
        }

        lastOcc.push_back(contours[0]->getNext(firstOcc[0], reverse[0]));
        lastOcc.push_back(contours[0]->getNext(firstOcc[1], reverse[1]));

        starLast.push_back(starFirst[0]->next);
        starLast.push_back(starFirst[1]->next);

        // Order of the contiguous lists of triangles found matters, each dealt with separately.
        if(!reverse[0] && !reverse[1])
        {
            CyclicList* newCycle = new CyclicList();

            while(firstOcc[1]->next != lastOcc[0])
            {
                newCycle->insert(firstOcc[1]->next->faceId);
                contours[0]->deleteAfter(firstOcc[1], false);
            }

            starFirst[1] = starFirst[1]->prev;
            while(starFirst[1] != starLast[0])
            {
                contours[0]->insertAfter(lastOcc[0], starFirst[1]->faceId, true);
                starFirst[1] = starFirst[1]->prev;
            }

           starFirst[0] = starFirst[0]->prev;
           while(starFirst[0] != starLast[1])
           {
               newCycle->insert(starFirst[0]->faceId);
               starFirst[0] = starFirst[0]->prev;
           }

           newCycle->contourNumber = contourNumber;
           contourNumber++;

           reebGraph->addSplitSaddle(mesh.position[v], contours[0]->contourNumber,
                                    newCycle->contourNumber, contourNumber);

           contours[0]->contourNumber = contourNumber;
           contourNumber++;

           cyclicLists.push_back(newCycle);
            
        }  else if (reverse[0] && reverse[1])
        {
            CyclicList* newCycle = new CyclicList();

            while(lastOcc[1]->next != firstOcc[0])
            {
                newCycle->insert(lastOcc[1]->next->faceId);
                contours[0]->deleteAfter(lastOcc[1], false);
            }

            starLast[0] = starLast[0]->next;
            while(starLast[0] != starFirst[1])
            {
                newCycle->insert(starLast[0]->faceId);
                starLast[0] = starLast[0]->next;
            }

            starFirst[0] = starFirst[0]->prev;

            while(starFirst[0] != starLast[1])
            {
                contours[0]->insertAfter(firstOcc[0], starFirst[0]->faceId, true);
                starFirst[0] = starFirst[0]->prev;
            }

            newCycle->contourNumber = contourNumber;
            contourNumber++;

            reebGraph->addSplitSaddle(mesh.position[v], contours[0]->contourNumber,
                                     newCycle->contourNumber, contourNumber);

            contours[0]->contourNumber = contourNumber;
            contourNumber++;

            cyclicLists.push_back(newCycle);
        } else if (reverse[0] && !reverse[1]){ // No new cycles created in this case

            CyclicList *newCycle = new CyclicList();

            while(starFirst[1] != starLast[0])
            {
                newCycle->insert(starFirst[1]->faceId);
                starFirst[1] = starFirst[1]->prev;
            }

            while(lastOcc[0] != lastOcc[1])
            {
                newCycle->insert(lastOcc[0]->faceId);
                lastOcc[0] = lastOcc[0]->prev;
            }

            while(starLast[1] != starFirst[0])
            {
                newCycle->insert(starLast[1]->faceId);
                starLast[1] = starLast[1]->next;
            }

            while(firstOcc[0] != firstOcc[1])
            {
                newCycle->insert(firstOcc[0]->faceId);
                firstOcc[0] = firstOcc[0]->next;
            }

            cyclicLists.erase(cyclicLists.begin() + contourId);

            newCycle->contourNumber = contourNumber;
            contourNumber++;
            cyclicLists.push_back(newCycle);

        } else
        {
            std::cout << "Problem with Mesh at saddle point." << std::endl;
        }

    // Contour MERGE condition
    } else if (contours.size() == 2) // If size is 2,  the contours merge together
    {
        // Delete trinagles in lower star
        for(size_t i = 0; i < firstOcc.size(); ++i)
        {
            while(contours[i]->getNext(contours[i]->getNext(firstOcc[i], reverse[i]), reverse[i])->faceId
                  == starFirst[i]->next->next->faceId)
            {
                contours[i]->deleteAfter(firstOcc[i], reverse[i]);
                star.deleteAfter(starFirst[i], false);

            }
        }

        lastOcc.push_back(contours[0]->getNext(firstOcc[0], reverse[0]));
        lastOcc.push_back(contours[0]->getNext(firstOcc[1], reverse[1]));

        starLast.push_back(starFirst[0]->next);
        starLast.push_back(starFirst[1]->next);

        starLast[0] = starLast[0]->next;

        // Build the new contour
        while(starLast[0] != starFirst[1])
        {
            contours[0]->insertAfter(firstOcc[0], starLast[0]->faceId,  reverse[0]);
            starLast[0] = starLast[0]->next;
        }

        while(firstOcc[1] != lastOcc[1])
        {
            contours[0]->insertAfter(firstOcc[0], firstOcc[1]->faceId,  reverse[0]);
            firstOcc[1] = contours[1]->getNext(firstOcc[1], !reverse[1]);
        }

        while(starLast[1] != starFirst[0])
        {
            contours[0]->insertAfter(firstOcc[0], starLast[1]->faceId,  reverse[0]);
            starLast[1] = starLast[1]->next;
        }

        reebGraph->addMergeSaddle(mesh.position[v], contours[0]->contourNumber,
                                    contours[1]->contourNumber, contourNumber);

        contours[0]->contourNumber = contourNumber;
        contourNumber++;

        // Delete the extra contour
        cyclicLists.erase(cyclicLists.begin() + contourId);

    }
    return true;
}

// Function called when a minimum vertex is reached
void Sweep::minimum(const Mesh &mesh, int v)
{
    CyclicList *newCycle = new CyclicList(contourNumber);
    newCycle->getStar(mesh, v);
    cyclicLists.push_back(newCycle);

    reebGraph->addMinimum(mesh.position[v], contourNumber);

    contourNumber++;
}

// Function called when a maximum vertex is reached
void Sweep::maximum(const Mesh &mesh, int v)
{
    CyclicList star = CyclicList();
    star.getStar(mesh, v);

    //std::cout << "star: ";
    //star.print();

    Node *contourFace = NULL;
    Node *starFace = star.last->next;
    int contourId = 0;

    do
    {
        contourId = 0;
        for(const auto &cycle : cyclicLists)
        {

            contourFace = cycle->find(starFace->faceId);

            if (contourFace!=NULL) {
                reebGraph->addMaximum(mesh.position[v], cycle->contourNumber);

                cyclicLists.erase(cyclicLists.begin() + contourId);
                break;
            }
            contourId++;
        }
        starFace = starFace->next;
    } while (starFace != star.last->next);
}

// Print contour cycles in the level-set for debugging
void Sweep::printCycles()
{
    if(cyclicLists.empty())
    {
        std::cout << "Empty level set" << std::endl;
        return;
    }

    std::cout << "Level set:" << std::endl;
    for(const auto &cycle : cyclicLists)
    {
        cycle->print();
    }
    std::cout << std::endl;
}

// Calculate the memory used the current level-set for evaluation
int Sweep::calculateMemoryUsage()
{
    if(cyclicLists.empty())
    {
        std::cout << "Empty level set" << std::endl;
        return  0;
    }

    int memoryUsed = 0; // Bytes

    for(const auto &cycle : cyclicLists)
    {
        memoryUsed += (cycle->getNumFaces() * 12) + 8;
    }

    if (peakMemoryUsed < memoryUsed)
    {
        peakMemoryUsed = memoryUsed;
    }
    return memoryUsed;
}



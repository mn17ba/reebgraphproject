#include "point3d.h"

Point3D::Point3D()
    : x(0.0f), y(0.0f), z(0.0f)
{

}

Point3D::Point3D(float x, float y, float z)
    : x(x), y(y), z(z)
{

}

#include <QMessageBox>

#include "window.h"

MainWindow::MainWindow(QWidget *parent)
	: QWidget(parent)
{
    setWindowTitle("Reeb graph computation");

    windowLayout = new QVBoxLayout(this);
    buttonLayout = new QHBoxLayout(this);

    // Draw display
    windowLayout->addSpacing(0);
    windowLayout->addStretch(1);

    display = new DisplayWidget(this);
    display->setMinimumHeight(512);
    display->setMaximumHeight(1024);
    display->setMinimumWidth(512);
    windowLayout->addWidget(display);

    // BUTTONS connected to slots
    fileButton = new QPushButton();
    fileButton->setText("Select Mesh");
    connect(fileButton, SIGNAL(released()), this, SLOT(handleFileButton()));
    buttonLayout->addWidget(fileButton);

    calcGeodesicsButton = new QPushButton();
    calcGeodesicsButton->setText("Calculate geodesics");
    connect(calcGeodesicsButton, SIGNAL(released()), display, SLOT(handleCalculateGeodesics()));
    buttonLayout->addWidget(calcGeodesicsButton);

    setGeodesicsButton = new QPushButton();
    setGeodesicsButton->setText("Set geodesics");
    connect(setGeodesicsButton, SIGNAL(released()), this, SLOT(handleSetGeodesics()));
    buttonLayout->addWidget(setGeodesicsButton);

    sweepButton = new QPushButton();
    sweepButton->setText("Sweep");
    connect(sweepButton, SIGNAL(released()), display, SLOT(handleSweep()));
    buttonLayout->addWidget(sweepButton);

    windowLayout->addLayout(buttonLayout);

    // Object orientation controls
    // Rotation sliders
    xAxisLabel = new QLabel(this);
    xAxisLabel->setText("Rotate object:");
    xAxisLabel->setMaximumHeight(25);
    windowLayout->addWidget(xAxisLabel, 0, Qt::AlignTop);

    xAxisSlider = new QSlider(Qt::Horizontal);
    xAxisSlider->setMinimum(0);
    xAxisSlider->setMaximum(360);
    windowLayout->addWidget(xAxisSlider, 0, Qt::AlignTop);
    connect(xAxisSlider, SIGNAL(valueChanged(int)), display, SLOT(updateAngleX(int)));

    yAxisSlider = new QSlider(Qt::Horizontal);
    yAxisSlider->setMinimum(0);
    yAxisSlider->setMaximum(360);
    windowLayout->addWidget(yAxisSlider, 0, Qt::AlignTop);
    connect(yAxisSlider, SIGNAL(valueChanged(int)), display, SLOT(updateAngleY(int)));

    zAxisSlider = new QSlider(Qt::Horizontal);
    zAxisSlider->setMinimum(0);
    zAxisSlider->setMaximum(360);
    windowLayout->addWidget(zAxisSlider, 0, Qt::AlignTop);
    connect(zAxisSlider, SIGNAL(valueChanged(int)), display, SLOT(updateAngleZ(int)));

    // Zoom slider
    zoomLabel = new QLabel(this);
    zoomLabel->setText("Zoom:");
    zoomLabel->setMaximumHeight(25);
    windowLayout->addWidget(zoomLabel, 0, Qt::AlignTop);

    zoomSlider = new QSlider(Qt::Horizontal);
    zoomSlider->setMinimum(1);
    zoomSlider->setMaximum(1000);
    zoomSlider->setValue(100);
    windowLayout->addWidget(zoomSlider, 0, Qt::AlignTop);
    connect(zoomSlider, SIGNAL(valueChanged(int)), display, SLOT(updateZoom(int)));
}

// Open file navigator to select a mesh from an OFF file
void MainWindow::handleFileButton()
{
    fileName = new QString(QFileDialog::getOpenFileName(this, "Open file", "C://"));
    //QMessageBox::information(this, "..", "File: " + *fileName);
    display->setFileName(*fileName);
}

// Open file navigator to select a mesh from a .geodesics file
void MainWindow::handleSetGeodesics()
{
    display->setGeodesicsFromFile(QFileDialog::getOpenFileName(this, "Open file", "C://"));
}

// Destroy all heap allocated objects
MainWindow::~MainWindow()
{
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    QWidget::closeEvent(event);
}

// resets all the interface elements
void MainWindow::ResetInterface()
{
    display->update();
	update();
}

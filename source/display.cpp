#if defined(__linux__)
  #include <GL/glu.h>
#elif defined(__APPLE__) && defined(__MACH__)
  #include <OpenGL/glu.h>
#endif

#include <QGLWidget>
#include <QDebug>
#include <iostream>
#include <QMessageBox>
#include <fstream>

#include "display.h"
#include "geodesics.h"
#include "sweep.h"

// Constructor - intializes all variables
DisplayWidget::DisplayWidget(QWidget *parent)
    : QGLWidget(parent)
{
    mesh = new Mesh();
    reebToggle = false;

    this->setFocusPolicy(Qt::ClickFocus);
}

DisplayWidget::~DisplayWidget()
{
    delete mesh;
}

// called when OpenGL context is set up
void DisplayWidget::initializeGL()
{
    glClearColor(0.8f, 0.9f, 1.0f, 1.0);

    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable( GL_BLEND );
}

// called every time the widget is resized
void DisplayWidget::resizeGL(int w, int h)
{
	// set the viewport to the entire widget
	glViewport(0, 0, w, h);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
    glOrtho(-5.0, 5.0, -5.0, 5.0, -5.0, 5.0);
}

// Called every time the widget needs painting
void DisplayWidget::paintGL()
{
    // Clear the widget
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glMatrixMode(GL_MODELVIEW);
    glEnable(GL_DEPTH_TEST);

    glColor3f(0.0,1.0,0.0);

    glRotatef(angleX, 1.0, 0.0, 0.0);
    glRotatef(angleY, 0.0, 1.0, 0.0);
    glRotatef(angleZ, 0.0, 0.0, 1.0);

    glScalef(zoom, zoom, zoom);

    if (reebToggle)
    {
        reebGraph->render();
    }
    mesh->render();

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    gluLookAt(1.,1.,1., 0.0,0.0,0.0, 0.0,1.0,0.0);

	// flush to screen
	glFlush();

}

// Set the filename of the mesh
void DisplayWidget::setFileName(QString fileName)
{
    this->fileName = fileName;

    if(!mesh->readOFFFile(fileName.toUtf8().constData()))
    {
        QMessageBox::critical(this, "Error", "Error reading file.");
    }

    reebToggle = false;
    update();
}

// Allows the user to set the geodesic function for a mesh by selected a .geodesics file
// in the file explorer
void DisplayWidget::setGeodesicsFromFile(QString fileName)
{
    if(mesh->position.empty())
    {
        QMessageBox::warning(this, "", "Select a mesh first");
    } else if(!mesh->readGeodesicsFile(fileName.toUtf8().constData())) {
        QMessageBox::warning(this, "Error", "Error reading file.");
    }

    reebToggle = false;
    update();
}


// SLOTS
// Slots for rotating the shape via the slider widget
void DisplayWidget::updateAngleY(int angle)
{
    angleY = angle;
    update();
}

void DisplayWidget::updateAngleX(int angle)
{
    angleX = angle;
    update();
}

void DisplayWidget::updateAngleZ(int angle)
{
    angleZ = angle;
    update();
}

// Slot for zooming into the shape via the zoom slider widget
void DisplayWidget::updateZoom(int zoom)
{
    this->zoom = (float) zoom / 100;
    update();
}


// Function that handles when the user selects the button to calculate the geodesics of a mesh.
void DisplayWidget::handleCalculateGeodesics()
{
    // Make sure a mesh has already been chosen by the user
    if(mesh->position.empty())
    {
        QMessageBox::warning(this, "Mesh not selected", "Mesh not selected");
        return;
    }

    // Initialise if not empty
    if(!mesh->geodesics.empty() || !mesh->colours.empty())
    {
        mesh->geodesics.clear();
        mesh->colours.clear();
        mesh->plFunction.clear();
    }

    // Perform the geodesics calculation on the mesh. Returns a bool value determining the success of the algorithm.
    if(!calculateGeodesics(*mesh))
    {
        QMessageBox::critical(this, "Geodesics calculation unsuccessful", "Geodesics calculation unsuccessful");
        return;
    } else {
        mesh->setHeatMapColours();
        update();
        QMessageBox::information(this, "Geodesics calculation successful", "Geodesics calculation successful");
    }

    reebToggle = false;
}

// Handles when the Sweep button is clicked to start the sweep algorithm
void DisplayWidget::handleSweep()
{
    if(mesh->geodesics.empty() || mesh->position.empty() || mesh->plFunction.empty())
    {
        QMessageBox::warning(this, "Error", "Mesh not ready: mesh, geodesics and critical points must be set");
        return;
    }

    // Identify the critical points in the mesh
    if(mesh->pointTypes.empty())
    {
        mesh->setPointTypes();
        mesh->setupPLFunction();
    }

    mesh->setTransparent();

    // Start the sweep algorithm
    Sweep sweep = Sweep();
    sweep.sweep(*mesh);
    reebGraph = sweep.reebGraph;
    reebToggle = true;

    update();
}



#if defined(__linux__)
  #include <GL/glu.h>
#elif defined(__APPLE__) && defined(__MACH__)
  #include <OpenGL/glu.h>
#endif

#include "reebgraph.h"
#include <iostream>

ReebGraph::ReebGraph()
{
}

// Add a minimum point node to the Reeb graph during the Sweep algorithm
void ReebGraph::addMinimum(const Point3D &pos, int arcOut)
{
    ReebNode *minNode = new ReebNode(nodes.size(), pos, PointType::minimum);

    minNode->arcs.push_back(arcOut);
    minNode->children.push_back(NULL);

    nodes.push_back(minNode);
}

// Add a maximum point node to the Reeb graph during the Sweep algorithm
void ReebGraph::addMaximum(const Point3D &pos, int arcIn)
{
    ReebNode *maxNode = new ReebNode(nodes.size(), pos, PointType::maximum);

    for(const auto &node : nodes)
    {
        for(size_t i = 0; i < node->arcs.size(); i++)
        {
            if(node->arcs[i] == arcIn)
            {
                node->children[i] = maxNode;
                nodes.push_back(maxNode);
                return;
            }
        }
    }
}

// Add a merge saddle point node to the Reeb graph during the Sweep algorithm
// Merge - two arcs in, one arc out
void ReebGraph::addMergeSaddle(const Point3D &pos, int arcIn1, int arcIn2, int arcOut)
{
    ReebNode *mergeNode = new ReebNode(nodes.size(), pos, PointType::saddle);

    mergeNode->arcs.push_back(arcOut);
    mergeNode->children.push_back(NULL);

    for(const auto &node : nodes)
    {
        for(size_t i = 0; i < node->arcs.size(); i++)
        {
            if(node->arcs[i] == arcIn1)
            {
                node->children[i] = mergeNode;

            } else if (node->arcs[i] == arcIn2)
            {
                node->children[i] = mergeNode;
            }
        }
    }

    nodes.push_back(mergeNode);
}

// Add a split saddle point node to the Reeb graph during the Sweep algorithm
// Split - one arcs in, two arcs out
void ReebGraph::addSplitSaddle(const Point3D &pos, int arcIn, int arcOut1, int arcOut2)
{
    ReebNode *splitNode = new ReebNode(nodes.size(), pos, PointType::saddle);

    splitNode->arcs.push_back(arcOut1);
    splitNode->arcs.push_back(arcOut2);
    splitNode->children.push_back(NULL);
    splitNode->children.push_back(NULL);

    for(const auto &node : nodes)
    {
        for(size_t i = 0; i < node->arcs.size(); i++)
        {
            if(node->arcs[i] == arcIn)
            {
                node->children[i] = splitNode;
                nodes.push_back(splitNode);
                return;
            }
        }
    }

}

// Add a twist saddle point node to the Reeb graph during the Sweep algorithm
// Twist - one arcs in, one arc out
void ReebGraph::addTwistSaddle(const Point3D &pos, int arcIn, int arcOut)
{
    ReebNode *node = new ReebNode(nodes.size(), pos, PointType::saddle);

    node->arcs.push_back(arcOut);
    node->children.push_back(NULL);

    for(const auto &node : nodes)
    {
        for(size_t i = 0; i < node->arcs.size(); i++)
        {
            if(node->arcs[i] == arcIn)
            {
                node->children[i] = node;
                nodes.push_back(node);
                return;
            }
        }
    }
}

// Render the Reeb graph in OpenGL
void ReebGraph::render()
{
    for(const auto &node : nodes)
    {
        switch(node->type)
        {
            case PointType::maximum:
                glColor3f(0,0,1);
                break;
            case PointType::saddle:
                glColor3f(0,1,0);
                break;
            case PointType::minimum:
                glColor3f(1,0,0);
                break;
            case PointType::regular:
                break;

        }

        glPushMatrix();

        glTranslatef(node->pos.x,node->pos.y,node->pos.z);

        // GLUT spheres uses to render balls representing the nodes
        GLUquadric *quad;
        quad = gluNewQuadric();

        gluSphere(quad,0.01f,100,20);

        glPopMatrix();


        // Draw the arcs between a parent node and its children
        glColor3f(0.0f, 0.0f, 0.0f);
        for(const auto &child : node->children)
        {
            if(child != NULL)
            {
                glLineWidth(3);
                // Draw line from node to child
                glBegin(GL_LINES);
                    glVertex3f(node->pos.x, node->pos.y, node->pos.z);
                    glVertex3f(child->pos.x, child->pos.y, child->pos.z);
                glEnd();
            }
        }
    }
}

// Print out the Reeb graph in full, used for debugging
void ReebGraph::print()
{
    for(const auto &node : nodes)
    {
        std::cout << "node id: " << node->nodeId << ", node loc: " << node << ", point type: " << (int) node->type << ", num children: " << node->children.size() << ", num arcs: " << node->arcs.size() << std::endl;
        for(size_t i = 0; i < node->arcs.size(); i++)
        {
            std::cout << "arc : " << node->arcs[i] << std::endl;
        }

        for(size_t i = 0; i < node->children.size(); i++)
        {
            if(node->children[i] == NULL)
                std::cout << "child : " << node->children[i] << std::endl;
            else
            {
                std::cout << "child : " << node->children[i]->nodeId << std::endl;
            }
        }
        std::cout << std::endl;

    }

}



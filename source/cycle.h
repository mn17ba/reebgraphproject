#ifndef CYCLE_H
#define CYCLE_H

#include "mesh.h"

// Class to represent the nodes in a contour cycle.
// nodes are doubly linked
class Node {
public:
    int faceId;
    Node *next;
    Node *prev;

    Node() :
        next(NULL) {};
    Node(int faceId) :
        faceId(faceId) {};
};

// Class to represent contour cycles in a level-set - implemented as a circular linked list
// Only a single pointer to the start of a linked list is required.
class CyclicList {
public:
    Node* last;             // Point the last node in the contour cycle
    int contourNumber;      // Unique number for each contour

    CyclicList();
    CyclicList(int contourNumber);
    ~CyclicList();

    // Functions to traverse the contour cycle
    Node* find(int faceId) const;
    Node* getNext(Node *node, bool reverse) const;
    Node* getPrev(Node *node, bool reverse) const;

    // Functions to alter the contour cycle
    Node* insertToEmpty(int faceId);
    Node* insert(int faceId);
    void insertAfter(Node* node, int faceId, bool reverse);
    void deleteAfter(Node* node, bool reverse);

    // Get the star of a vertex in the form of a contour cycle
    bool getStar(const Mesh &mesh, int v);

    // Functions for printing and debugging purposes
    void print() const;
    void printFull() const;
    int getNumFaces() const;
};

#endif // CYCLE_H

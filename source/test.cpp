#include <iostream>

#include "mesh.h"
#include "cycle.h"
#include "sweep.h"

int main(int argc, char *argv[]) {

	Mesh *mesh = new Mesh();

	if(!mesh->readOFFFile("models/tetrahedron.off"))
    {
        std::cout << "error reading file" << std::endl;
    }

	Sweep sweep = Sweep();
    //sweep.sweep(*mesh);

	sweep.saddle(*mesh, 0);

	return 0;
}

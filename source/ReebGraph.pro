######################################################################
######################################################################

TEMPLATE = app
TARGET = ReebGraph
INCLUDEPATH += . /opt/local/include

CONFIG += c++11

QT += widgets opengl gui

unix:!macx {
  LIBS += -lGLU -lglut #DEC-10
}

# Input
HEADERS += \
    cycle.h \
    mesh.h \
    reebgraph.h \
    sweep.h \
    window.h \
    display.h \
    point3d.h \
    geodesics.h

SOURCES += \
    cycle.cpp \
    main.cpp \
    mesh.cpp \
    reebgraph.cpp \
    sweep.cpp \
    window.cpp \
    display.cpp \
    point3d.cpp \
    geodesics.cpp

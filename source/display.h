#ifndef DISPLAY_H
#define DISPLAY_H

#include <QGLWidget>
#include <QString>
#include <QKeyEvent>

#include "reebgraph.h"
#include "mesh.h"
#include "geodesics.h"

// Widget for displaying the OpenGL scene
// allows for shapes to be rendered inside of it
class DisplayWidget: public QGLWidget {
Q_OBJECT
public:
    DisplayWidget(QWidget *parent);
    ~DisplayWidget();

    void setFileName(QString fileName);
    void setGeodesicsFromFile(QString fileName);

protected:
	void initializeGL();
	void resizeGL(int w, int h);
    void paintGL();

public slots:
    void updateAngleY(int angle);
    void updateAngleX(int angle);
    void updateAngleZ(int angle);
    void updateZoom(int zoom);
    void handleCalculateGeodesics();
    void handleSweep();

    /* display methods */
private:
    Mesh *mesh;
    ReebGraph *reebGraph;

    float angleY = 0, angleX = 0, angleZ = 0;
    float zoom = 1.0f;
    GLfloat cameraPos[3];
    QString fileName;
    bool reebToggle;

};
	
#endif

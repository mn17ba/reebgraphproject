#ifndef REEBGRAPH_H
#define REEBGRAPH_H

#include "point3d.h"
#include "cycle.h"
#include <vector>

// The nodes in the reeb graph
struct ReebNode
{
    int nodeId;
    Point3D pos;
    std::vector<ReebNode*> children;
    std::vector<int> arcs;
    PointType type;


    ReebNode(int id, const Point3D &pos, PointType type)
        : nodeId(id), pos(pos), type(type) {}
};

// The Reeb graph, a collection of nodes
class ReebGraph
{
public:
    std::vector<ReebNode*> nodes;
    ReebGraph();

    // Functions to help build the Reeb graph
    void addMinimum(const Point3D &pos, int arcOut);
    void addMaximum(const Point3D &pos, int arcIn);
    void addMergeSaddle(const Point3D &pos, int arcIn1, int arcIn2, int arcOut);
    void addSplitSaddle(const Point3D &pos, int arcIn, int arcOut1, int arcOut2);
    void addTwistSaddle(const Point3D &pos, int arcIn, int arcOut);

    void render();

    // For debugging
    void print();

};

#endif // REEBGRAPH_H
